# Appreciate IT Shopping Cart Service

## API Catalog
None -- Swagger Links to be provided.
## Deployment Instructions
None
## Service Dependencies
Postgres DB

## REST Endpoints
* POST /v0/products
* GET /v0/products
* GET /v0/products/{id}
* POST /v0/orders
* GET /v0/orders
* GET /v0/orders/{id}