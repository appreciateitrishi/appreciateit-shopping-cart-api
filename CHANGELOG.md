# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1]
* Added POST /v0/products endpoint
* Added POST /V0/orders endpoint
* Added GET /V0/orders/ endpoint
* Added GET /V0/orders/{id} endpoint
* Added GET /V0/orders/ endpoint


## [1.0]
* Added GET /v0/products endpoint
* Added GET /V0/products/{id} endpoint
* Added Connectivity to Postgres DB

## [0.1]
* Added  Initial Project Setup