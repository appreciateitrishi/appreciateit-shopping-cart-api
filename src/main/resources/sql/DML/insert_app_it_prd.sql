	INSERT INTO APP_IT_PRD values
	(1, 'Dell Monitor', 'Dell 24 inch Monitor', 'Electronics', 'DELLMN24IN2');

	INSERT INTO APP_IT_PRD values
	(2, 'NorthFace Jacket', 'Parker Jacket', 'Winter Clothing', 'NFJP34');

	INSERT INTO APP_IT_PRD values
	(3, 'Alphabet T-Shirt', 'T-Shirt Medium Size', 'Clothing', 'ALPHTSHM594');

	INSERT INTO APP_IT_PRD values
	(4, 'Apple iPhoneXR', 'iPhoneXR 128 GB', 'Phones and Tablets', 'APLIPXR128');
