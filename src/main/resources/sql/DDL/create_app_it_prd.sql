-- Table: public.app_it_prd

-- DROP TABLE public.app_it_prd;

CREATE TABLE public.app_it_prd
(
    prd_id bigint NOT NULL DEFAULT nextval('app_it_prd_prd_id_seq'::regclass),
    prd_name character varying(200) COLLATE pg_catalog."default",
    prd_desc character varying(200) COLLATE pg_catalog."default",
    prd_category character varying(200) COLLATE pg_catalog."default",
    prd_model_num character varying(50) COLLATE pg_catalog."default",
    prd_price numeric,
    CONSTRAINT app_it_prd_pkey PRIMARY KEY (prd_id)
)

TABLESPACE pg_default;

ALTER TABLE public.app_it_prd
    OWNER to postgres;

COMMENT ON COLUMN public.app_it_prd.prd_id
    IS 'Unique Identifier';