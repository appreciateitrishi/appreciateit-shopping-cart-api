-- Table: public.app_it_ord_prd

-- DROP TABLE public.app_it_ord_prd;

CREATE TABLE public.app_it_ord_prd
(
    ord_id bigint NOT NULL,
    prd_id bigint NOT NULL,
    qty integer NOT NULL DEFAULT 1,
    total_price numeric,
    CONSTRAINT app_it_ord_prd_pkey PRIMARY KEY (ord_id, prd_id),
    CONSTRAINT app_it_ord_fkey FOREIGN KEY (ord_id)
        REFERENCES public.app_it_ord (ord_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "app_it_prd_fKey" FOREIGN KEY (prd_id)
        REFERENCES public.app_it_prd (prd_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.app_it_ord_prd
    OWNER to postgres;

-- Index: fki_app_it_prd_fKey

-- DROP INDEX public."fki_app_it_prd_fKey";

CREATE INDEX "fki_app_it_prd_fKey"
    ON public.app_it_ord_prd USING btree
    (prd_id)
    TABLESPACE pg_default;

-- Index: fki_ord_fk

-- DROP INDEX public.fki_ord_fk;

CREATE INDEX fki_ord_fk
    ON public.app_it_ord_prd USING btree
    (ord_id)
    TABLESPACE pg_default;