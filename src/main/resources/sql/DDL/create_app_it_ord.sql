-- Table: public.app_it_ord

-- DROP TABLE public.app_it_ord;

CREATE TABLE public.app_it_ord
(
    ord_id bigint NOT NULL DEFAULT nextval('app_it_ord_ord_id_seq'::regclass),
    ord_confirm_num character varying(50) COLLATE pg_catalog."default",
    ord_placed_by character varying(250) COLLATE pg_catalog."default",
    ord_shipping_add character varying(250) COLLATE pg_catalog."default",
    ord_status character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT app_it_order_pkey PRIMARY KEY (ord_id)
)

TABLESPACE pg_default;

ALTER TABLE public.app_it_ord
    OWNER to postgres;