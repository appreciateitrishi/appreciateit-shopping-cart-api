package tech.appreciateit.shoppingcart.orders;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.math.BigDecimal;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "app_it_ord_prd")
public class OrderProduct implements Serializable {

    @EmbeddedId
    private OrderProductKey key;

    @Column(name = "qty")
    private Integer quantity;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

}
