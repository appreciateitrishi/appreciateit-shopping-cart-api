package tech.appreciateit.shoppingcart.orders;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.appreciateit.shoppingcart.products.Product;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderedProductDTO extends Product {
    private Integer quantity;
    private BigDecimal totalPrice;
}
