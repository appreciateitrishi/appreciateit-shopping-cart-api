package tech.appreciateit.shoppingcart.orders;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ModelMapper orderMapper;

    public OrderController(OrderService orderService, ModelMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    @GetMapping(value = "v0/orders")
    public List<OrderDTO> getOrders() {
        return ofNullable(orderService.getOrders())
                .orElse(Collections.emptyList())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "v0/orders")
    public OrderDTO createOrder(@RequestBody OrderDTO orderDTO) {
        Order order = getOrder(orderDTO);
        return ofNullable(orderService.saveOrder(order))
                .map(this::convertToDTO)
                .orElse(null);
    }

    @GetMapping(value = "v0/orders/{id}")
    public OrderDTO getOrder(@PathVariable String id) {
        return ofNullable(orderService.getOrder(Long.parseLong(id)))
                .map(this::convertToDTO)
                .orElse(null);
    }

    private Order getOrder(@RequestBody OrderDTO orderDTO) {
        Order order = new Order();
        order.setConfirmNum(orderDTO.getConfirmNum());
        order.setPlacedBy(orderDTO.getPlacedBy());
        order.setShippingAdd(orderDTO.getShippingAdd());
        order.setStatus("SUBMITTED");

        for (OrderedProductDTO orderedProductDTO : orderDTO.getProducts()) {
            OrderProductKey key = OrderProductKey.builder()
                    .order(order)
                    .product(orderedProductDTO)
                    .build();

            OrderProduct orderProduct = OrderProduct.builder()
                    .key(key)
                    .quantity(orderedProductDTO.getQuantity())
                    .build();
            order.getOrderProducts().add(orderProduct);
        }
        return order;
    }

    private OrderDTO convertToDTO(Order order) {
        OrderDTO orderDTO = orderMapper.map(order, OrderDTO.class);
        Set<OrderProduct> orderProducts = order.getOrderProducts();
        if (orderProducts != null && !orderProducts.isEmpty()) {
            //TODO: Code refactoring is needed to avoid deep mapping from mapper
            orderDTO.setProducts(new HashSet<OrderedProductDTO>());
            for (OrderProduct orderProduct : orderProducts) {
                OrderedProductDTO orderedProductDTO = orderMapper.map(orderProduct.getKey().getProduct(), OrderedProductDTO.class);
                orderedProductDTO.setQuantity(orderProduct.getQuantity());
                orderedProductDTO.setTotalPrice(orderProduct.getTotalPrice());
                orderDTO.getProducts().add(orderedProductDTO);
            }
        }
        return orderDTO;
    }
}
