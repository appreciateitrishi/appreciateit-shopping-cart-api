package tech.appreciateit.shoppingcart.orders;

import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO implements Serializable {
    private Long id;
    private String confirmNum;
    private String placedBy;
    private String shippingAdd;
    private String status;

    private Set<OrderedProductDTO> products = new HashSet<OrderedProductDTO>();
}
