package tech.appreciateit.shoppingcart.orders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.util.Optional.ofNullable;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order getOrder(long id) {
        return this.orderRepository.findById(id)
                .orElse(null);
    }

    public List<Order> getOrders() {
        List<Order> orders = new ArrayList<>();
        ofNullable(this.orderRepository.findAll())
                .ifPresent(i -> i.forEach(order -> orders.add(order)));
        return orders;
    }

    public Order saveOrder(Order order) {
        return this.orderRepository.save(order);
    }

}
