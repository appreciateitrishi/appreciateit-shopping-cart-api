package tech.appreciateit.shoppingcart.orders;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(name = "app_it_ord")
public class Order implements Serializable {

    @Id
    @SequenceGenerator(name = "app_it_ord_ord_id_seq", sequenceName = "app_it_ord_ord_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_it_ord_ord_id_seq")
    @Column(name = "ord_id")
    private Long id;

    @Column(name = "ord_confirm_num")
    private String confirmNum;

    @Column(name = "ord_placed_by")
    private String placedBy;

    @Column(name = "ord_shipping_add")
    private String shippingAdd;

    @Column(name = "ord_status")
    private String status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "key.order")
    private Set<OrderProduct> orderProducts = new HashSet<OrderProduct>();
}
