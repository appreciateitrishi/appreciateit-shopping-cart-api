package tech.appreciateit.shoppingcart.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.util.Optional.ofNullable;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product getProduct(long id) {
        return this.productRepository.findById(id)
                .orElse(null);
    }

    public Product saveProduct(Product product) {
        return this.productRepository.save(product);
    }

    public List<Product> getProducts() {
        List<Product> products = new ArrayList<>();
        ofNullable(this.productRepository.findAll())
                .ifPresent(i -> i.forEach(product -> products.add(product)));
        return products;
    }
}
