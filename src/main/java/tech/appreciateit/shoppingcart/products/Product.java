package tech.appreciateit.shoppingcart.products;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "app_it_prd")
public class Product implements Serializable {

    @Id
    @SequenceGenerator(name = "app_it_prd_prd_id_seq", sequenceName = "app_it_prd_prd_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_it_prd_prd_id_seq")
    @Column(name = "prd_id")
    private Long id;

    @Column(name = "prd_name")
    private String name;

    @Column(name = "prd_desc")
    private String description;

    @Column(name = "prd_category")
    private String category;

    @Column(name = "prd_model_num")
    private String modelNumber;

    @Column(name = "prd_price")
    private BigDecimal unitPrice;

}
