package tech.appreciateit.shoppingcart.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "v0/products")
    public List<Product> getProducts() {
        return productService.getProducts();
    }

    @GetMapping(value = "v0/products/{id}")
    public Product getProduct(@PathVariable String id) {
        return productService.getProduct(Long.parseLong(id));
    }

    @PostMapping(value = "v0/products")
    public Product createProduct(@RequestBody Product product) {
        return productService.saveProduct(product);
    }
}
