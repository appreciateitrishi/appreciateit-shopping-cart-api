package tech.appreciateit.shoppingcart.products;

//@AutoConfigureMockMvc
//@ActiveProfiles({"profiles", "test"})
public class ProductsIT {

/*
    @Autowired
    private MockMvc mvc;

    @MockBean
    ProductRepository mockProductRepository;

    @InjectMocks
    ProductService mockProductService;

    String jsonOutput = "[\n" +
            "  {\n" +
            "    \"id\": 1,\n" +
            "    \"name\": \"Dell Monitor\",\n" +
            "    \"description\": \"Dell 24 inch Monitor\",\n" +
            "    \"category\": \"Electronics\",\n" +
            "    \"modelNumber\": \"DELLMN24IN2\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 2,\n" +
            "    \"name\": \"NorthFace Jacket\",\n" +
            "    \"description\": \"Parker Jacket\",\n" +
            "    \"category\": \"Winter Clothing\",\n" +
            "    \"modelNumber\": \"NFJP34\"\n" +
            "  }\n" +
            "]";


    @BeforeEach
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        Mockito.doReturn(getMockedProducts()).when(mockProductRepository).findAll();
    }


    @Test
    public void getProducts_hasProducts_returnsValidResponse() {
        executeGet("v0/products/");
    }

    @SneakyThrows
    private void executeGet(String url) {
        mvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(jsonOutput));

    }

        public List<Product> getMockedProducts() {
            Product product1 = Product.builder()
                    .id(1L)
                    .name("Dell Monitor")
                    .description("Dell 24 inch Monito")
                    .category("Electronics")
                    .modelNumber("DELLMN24IN2")
                    .build();

            Product product2 = Product.builder()
                    .id(2L)
                    .name("NorthFace Jacket")
                    .description("Parker Jacket")
                    .category("Winter Clothing")
                    .modelNumber("NFJP34")
                    .build();
            return Arrays.asList(product1, product2);
    }
*/

}
