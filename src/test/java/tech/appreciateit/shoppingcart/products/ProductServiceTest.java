package tech.appreciateit.shoppingcart.products;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import tech.appreciateit.shoppingcart.BaseTest;

import java.util.List;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest extends BaseTest {

    private long productId = 1L;

    @Mock
    ProductRepository mockProductRepository;
    @InjectMocks
    ProductService productService;

    @Test
    public void testGetProduct_whenProductFound_returnsValidResponse() {
        //Given
        Product expectedProduct = getProduct1();
        doReturn(of(getMockedProduct(productId))).when(mockProductRepository).findById(productId);
        //When
        Product actualProduct = productService.getProduct(productId);
        //Then
        assertProduct(expectedProduct, actualProduct);
    }

    @Test
    public void testGetProduct_whenProductNotFound_returnsProductAsNull() {
        //Given
        doReturn(empty()).when(mockProductRepository).findById(productId);
        //When
        Product product = productService.getProduct(productId);
        //Then
        assertEquals(product, null);
    }

    @Test
    public void testGetProducts_whenProductsFound_returnsValidResponse() {
        //Given
        List<Product> expectedProducts = getMockedProducts();
        doReturn((Iterable<Product>) expectedProducts).when(mockProductRepository).findAll();
        //When
        List<Product> actualProducts = productService.getProducts();
        //Then
        assertEquals(expectedProducts.size(), actualProducts.size());
        assertProduct(expectedProducts.get(0), actualProducts.get(0));
        assertProduct(expectedProducts.get(1), actualProducts.get(1));
    }

    @Test
    public void testGetProducts_whenProductNotFound_returnsEmptyList() {
        //Given
        doReturn(null).when(mockProductRepository).findAll();
        //When
        List<Product> actualProducts = productService.getProducts();
        //Then
        assertTrue(actualProducts.isEmpty());
    }

}
