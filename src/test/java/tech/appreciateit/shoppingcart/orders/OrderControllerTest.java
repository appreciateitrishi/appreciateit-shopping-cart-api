package tech.appreciateit.shoppingcart.orders;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import tech.appreciateit.shoppingcart.BaseTest;
import tech.appreciateit.shoppingcart.products.Product;
import tech.appreciateit.shoppingcart.products.ProductController;
import tech.appreciateit.shoppingcart.products.ProductService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;

//@RunWith(MockitoJUnitRunner.class)
public class OrderControllerTest extends BaseTest {

    private long productId = 1L;


/*    @Mock
    ProductService mockProductService;
    @InjectMocks
    ProductController productController;

    @Test
    public void testGetProduct_whenProductFound_returnsValidResponse() {
        //Given
        Product expectedProduct = getProduct1();
        doReturn(expectedProduct).when(mockProductService).getProduct(productId);
        //When
        Product actualProduct = productController.getProduct(String.valueOf(productId));
        //Then
        assertProduct(expectedProduct, actualProduct);
    }

    @Test
    public void testGetProduct_whenProductNotFound_returnsProductAsNull() {
        //Given
        doReturn(null).when(mockProductService).getProduct(productId);
        //When
        Product product = productController.getProduct(String.valueOf(productId));
        //Then
        assertEquals(product, null);
    }

    @Test
    public void testGetProducts_whenProductsFound_returnsValidResponse() {
        //Given
        List<Product> expectedProducts = getMockedProducts();
        doReturn(expectedProducts).when(mockProductService).getProducts();
        //When
        List<Product> actualProducts = productController.getProducts();
        //Then
        assertEquals(expectedProducts.size(), actualProducts.size());
        assertProduct(expectedProducts.get(0), actualProducts.get(0));
        assertProduct(expectedProducts.get(1), actualProducts.get(1));
    }

    @Test
    public void testGetProducts_whenProductNotFound_returnsEmptyList() {
        //Given
        doReturn(new ArrayList<Product>()).when(mockProductService).getProducts();
        //When
        List<Product> actualProducts = productController.getProducts();
        //Then
        assertTrue(actualProducts.isEmpty());
    }*/

}
