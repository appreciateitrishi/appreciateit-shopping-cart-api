package tech.appreciateit.shoppingcart;

import tech.appreciateit.shoppingcart.orders.Order;
import tech.appreciateit.shoppingcart.products.Product;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static java.util.Optional.ofNullable;
import static org.junit.Assert.assertEquals;

public class BaseTest {
    public Product getMockedProduct(long id) {
        List<Product> products = getMockedProducts();
        return ofNullable(products)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(p -> p.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Product> getMockedProducts() {
        Product product1 = Product.builder()
                .id(1L)
                .name("Dell Monitor")
                .description("Monitor")
                .category("Electronics")
                .modelNumber("DELLMN1")
                .build();

        Product product2 = Product.builder()
                .id(2L)
                .name("NorthFace Jacket")
                .description("Parker Jacket")
                .category("Winter Clothing")
                .modelNumber("NFJP34")
                .build();
        return Arrays.asList(product1, product2);
    }

    public Product getProduct1() {
        return Product.builder()
                .id(1L)
                .name("Dell Monitor")
                .description("Monitor")
                .category("Electronics")
                .modelNumber("DELLMN1")
                .build();
    }

    public Product getProduct2() {
        return Product.builder()
                .id(2L)
                .name("NorthFace Jacket")
                .description("Parker Jacket")
                .category("Winter Clothing")
                .modelNumber("NFJP34")
                .build();
    }

    protected void assertProduct(Product expectedProduct, Product actualProduct) {
        assertEquals(expectedProduct.getId(), actualProduct.getId());
        assertEquals(expectedProduct.getCategory(), actualProduct.getCategory());
        assertEquals(expectedProduct.getDescription(), actualProduct.getDescription());
        assertEquals(expectedProduct.getModelNumber(), actualProduct.getModelNumber());
        assertEquals(expectedProduct.getName(), actualProduct.getName());
    }


/*
    public Order getMockedOrder(long id) {
        List<Order> orders = getMockedOrders();
        return ofNullable(orders)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(p -> p.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Order> getMockedOrders() {
        Order order1 = Order.builder()
                .id(1L)
                .confirmNum("ABCD123")
                .placedBy("Mike J")
                .shippingAdd("123 King St. Toronto, ON M5V 2K0")
                .status("SUBMITTED")
                .products(new HashSet<>(getMockedProduct()))
                .build();

        Order order2 = Order.builder()
                .id(2L)
                .name("NorthFace Jacket")
                .description("Parker Jacket")
                .category("Winter Clothing")
                .modelNumber("NFJP34")
                .build();
        return Arrays.asList(order1, order2);
    }

    public Order getOrder1() {
        return Order.builder()
                .id(1L)
                .name("Dell Monitor")
                .description("Monitor")
                .category("Electronics")
                .modelNumber("DELLMN1")
                .build();
    }

    public Order getOrder2() {
        return Order.builder()
                .id(2L)
                .name("NorthFace Jacket")
                .description("Parker Jacket")
                .category("Winter Clothing")
                .modelNumber("NFJP34")
                .build();
    }

    protected void assertOrder(Order expectedOrder, Order actualOrder) {
        assertEquals(expectedOrder.getId(), actualOrder.getId());
        assertEquals(expectedOrder.getCategory(), actualOrder.getCategory());
        assertEquals(expectedOrder.getDescription(), actualOrder.getDescription());
        assertEquals(expectedOrder.getModelNumber(), actualOrder.getModelNumber());
        assertEquals(expectedOrder.getName(), actualOrder.getName());
    }*/

}
